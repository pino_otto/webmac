# Open al*arm

Here is the list of the sub-projects that constitute Open al*arm (code named WebMac):

*  [mac-board](https://gitlab.com/pino_otto/mac-board)
*  [mac-arduino](https://gitlab.com/pino_otto/mac-arduino)
*  [modbusdriver](https://gitlab.com/pino_otto/modbusdriver)
*  [mac-commons](https://gitlab.com/pino_otto/mac-commons)
*  [mac-db](https://gitlab.com/pino_otto/mac-db)
*  [mac-logger](https://gitlab.com/pino_otto/mac-logger)
*  [mac-web-play](https://gitlab.com/pino_otto/mac-web-play)
*  [mac-register-db](https://gitlab.com/pino_otto/mac-register-db)
*  [mac-register-server](https://gitlab.com/pino_otto/mac-register-server)
*  [mac-register-client](https://gitlab.com/pino_otto/mac-register-client)
  
For any comments or suggestions, please create an "issue" here: [issues](https://gitlab.com/pino_otto/webmac/issues)
